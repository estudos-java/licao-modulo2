import java.util.Scanner;

public class Menu {
    public static void main(String[] args) {
        
        Scanner entrada = new Scanner(System.in);
        int opcoes;

        do{
        System.out.println("|                   MENU DE OPÇÕES                   |");
        System.out.println("|                                                    |");
        System.out.println("|1-OPÇÃO 1                                           |");
        System.out.println("|2-OPÇÃO 2                                           |");
        System.out.println("|3-SAIR                                              |");

        System.out.println("\nDIGITE A OPÇÃO CORRETA: ");

        opcoes = entrada.nextInt();

        switch (opcoes) {

            case 1:
                    System.out.println("\nVocê escolheu a primeira opção! \n\n");
            break;

            case 2:
                    System.out.println("\nVocê escolheu a segunda opção! \n\n");
            break;

            default: 
                    if(opcoes > 3){
                        System.out.println("\nOpção inválida, tente novamente! \n\n");                  
            break;
             }

         }
        }while (opcoes != 3);
        System.out.println("\nObrigado por usar nosso sistema, o programa foi encerrado. Volte sempre! \n\n");
    }
}
