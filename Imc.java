import java.util.Scanner;

public class Imc {

    public static void main(String[] args) {

        System.out.println("\nBEM VINDO AO CALCULADOR DE MASSA CORPORAL - IMC!");
        System.out.println("Por gentileza, digite o seu peso em Kg: ");
        Scanner entrada = new Scanner(System.in);
        Float peso = entrada.nextFloat();

        System.out.println("Muito bem, agora para finalizar, a sua altura em metros: ");
        Scanner entrada2 = new Scanner(System.in);
        Float altura = entrada2.nextFloat() * 2;
        Float resultado = peso / altura;

        System.out.println("Calculando.......\n");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.printf("O seu IMC é: "+"%.2f", resultado);

        if(resultado < 18.5){
            System.out.println("Kg/m², e esta classificado em: Baixo peso.");
        }else if((resultado >= 18.5) && (resultado <= 24.9)){
            System.out.println("Kg/m², e esta classificado em: Intervalo normal.");
        }else if((resultado >= 25) && (resultado <= 29.9)){
            System.out.println("Kg/m², e esta classificado em: Sobrepeso.");
        }else if((resultado >= 30) && (resultado <= 34.9)){
            System.out.println("Kg/m², e esta classificado em: Obesidade classe I.");
        }else if((resultado >= 35) && (resultado <= 39.9)){
            System.out.println("Kg/m², e esta classificado em: Obesidade classe II.");
        }else if(resultado >= 40){
            System.out.println("Kg/m², e esta classificado em: Obesidade classe III.");
        }


        System.out.println("\n\n               Segue tabela de referência, conforme ANS: \n");
 
        System.out.println("|                      VALORES DE REFERÊNCIAS                      |");
        System.out.println("|     IMC                                          DIAGNÓSTICO     |");
        System.out.println("|Menor que 18,5    --------------------------- Baixo peso          |");
        System.out.println("|Entre 18,5 e 24,9 --------------------------- Intervalo normal    |");
        System.out.println("|Entre 25 e 29,9   --------------------------- Sobrepeso           |");
        System.out.println("|Entre 30 e 34,9   --------------------------- Obesidade classe I  |");
        System.out.println("|Entre 35 e 39,9   --------------------------- Obesidade classe II |");
        System.out.println("|Maior que 40      --------------------------- Obesidade classe III|");
        System.out.println("Fonte: https://aps.bvs.br/apps/calculadoras/?page=6 \n\n");



        
    
    }
    
}
